USE reaktor_chat_service_login;
INSERT INTO reaktor_chat_service_login.user_details (user_uuid, email, username, first_name, last_name, birthdate, registered, gender) VALUES (GEN_BIN_UUID(), 'user@example.com', 'admin', 'App', 'Administrator', UNIX_TIMESTAMP(STR_TO_DATE('1.1.1990', '%d.%m.%Y')), UNIX_TIMESTAMP(NOW()), b'1');
SELECT CONV_BIN_TXT_UUID(user_uuid) FROM user_details WHERE username = 'admin';
INSERT INTO password (user_uuid, password) VALUES ((SELECT user_uuid FROM user_details WHERE username = 'admin'), SHA1('vengenfulSpirit'));
USE reaktor_chat_service_app;
INSERT INTO authorized_users (user_uuid, permit_login, is_admin, is_debuguser, is_banned) VALUES ((SELECT user_uuid FROM reaktor_chat_service_login.user_details WHERE username = 'admin'), b'1', b'1', b'0', b'0');

