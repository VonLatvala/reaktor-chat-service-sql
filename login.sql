-- --------------------------------------------------------
-- Verkkotietokone:              127.0.0.1
-- Palvelinversio:               5.5.52-0+deb8u1 - (Debian)
-- Server OS:                    debian-linux-gnu
-- HeidiSQL Versio:              9.3.0.5107
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for reaktor_chat_service_login
CREATE DATABASE IF NOT EXISTS `reaktor_chat_service_login` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;
USE `reaktor_chat_service_login`;

-- Dumping structure for function reaktor_chat_service_login.CONV_BIN_TXT_UUID
DELIMITER //
CREATE DEFINER=`root`@`localhost` FUNCTION `CONV_BIN_TXT_UUID`(`BIN_UUID` BINARY(16)) RETURNS char(36) CHARSET utf8
    NO SQL
BEGIN
RETURN concat(HEX(LEFT(BIN_UUID,4)),'-', HEX(MID(BIN_UUID,5,2)),'-', HEX(MID(BIN_UUID,7,2)),'-',HEX(MID(BIN_UUID,9,2)),'-',HEX(RIGHT(BIN_UUID,6))); 
END//
DELIMITER ;

-- Dumping structure for function reaktor_chat_service_login.CONV_TXT_BIN_UUID
DELIMITER //
CREATE DEFINER=`root`@`localhost` FUNCTION `CONV_TXT_BIN_UUID`(`TXT_UUID` CHAR(36)) RETURNS binary(16)
    NO SQL
BEGIN
RETURN CONCAT(UNHEX(LEFT(TXT_UUID,8)),UNHEX(MID(TXT_UUID,10,4)),UNHEX(MID(TXT_UUID,15,4)),UNHEX(MID(TXT_UUID,20,4)),UNHEX(RIGHT(TXT_UUID,12))); 
END//
DELIMITER ;

-- Dumping structure for function reaktor_chat_service_login.GEN_BIN_UUID
DELIMITER //
CREATE DEFINER=`root`@`localhost` FUNCTION `GEN_BIN_UUID`() RETURNS binary(16)
    NO SQL
BEGIN
DECLARE TXT_UUID char(37);
SET TXT_UUID = UUID();
RETURN CONV_TXT_BIN_UUID(TXT_UUID);
END//
DELIMITER ;

-- Dumping structure for taulu reaktor_chat_service_login.password
CREATE TABLE IF NOT EXISTS `password` (
  `user_uuid` binary(16) NOT NULL,
  `password` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`user_uuid`),
  CONSTRAINT `FK_password_user_details` FOREIGN KEY (`user_uuid`) REFERENCES `user_details` (`user_uuid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Tietojen vientiä ei oltu valittu.
-- Dumping structure for taulu reaktor_chat_service_login.user_details
CREATE TABLE IF NOT EXISTS `user_details` (
  `user_uuid` binary(16) NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `username` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `first_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `birthdate` int(11) DEFAULT NULL,
  `registered` int(11) DEFAULT NULL,
  `gender` binary(1) DEFAULT NULL,
  PRIMARY KEY (`user_uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Tietojen vientiä ei oltu valittu.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
