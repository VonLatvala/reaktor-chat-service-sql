# Reaktor Chat Service SQL Support #

### What is this repository for? ###

* All the SQL you need to initialize the database for [Reaktor Chat Service](https://bitbucket.org/VonLatvala/reaktor-chat-service)
* A live version of the app is available at https://reaktor-chat-service.alatvala.fi
* Version 0.1

### How do I get set up? ###

* Use Vagrant, there is a repo for the vagrant and bootstrap files [here](https://bitbucket.org/VonLatvala/reaktor-chat-service-vagrant)

### Who do I talk to? ###

* Repo owner (Axel Latvala)