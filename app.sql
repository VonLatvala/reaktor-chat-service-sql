-- --------------------------------------------------------
-- Verkkotietokone:              127.0.0.1
-- Palvelinversio:               5.5.52-0+deb8u1 - (Debian)
-- Server OS:                    debian-linux-gnu
-- HeidiSQL Versio:              9.3.0.5107
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for reaktor_chat_service_app
CREATE DATABASE IF NOT EXISTS `reaktor_chat_service_app` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;
USE `reaktor_chat_service_app`;

-- Dumping structure for taulu reaktor_chat_service_app.authorized_users
CREATE TABLE IF NOT EXISTS `authorized_users` (
  `user_uuid` binary(16) NOT NULL,
  `permit_login` bit(1) NOT NULL DEFAULT b'1',
  `is_admin` bit(1) NOT NULL DEFAULT b'0',
  `is_debuguser` bit(1) NOT NULL DEFAULT b'0',
  `is_banned` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`user_uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Tietojen vientiä ei oltu valittu.
-- Dumping structure for taulu reaktor_chat_service_app.chats
CREATE TABLE IF NOT EXISTS `chats` (
  `chat_uuid` binary(16) NOT NULL,
  `name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `type` tinyint(4) NOT NULL,
  `is_public` bit(1) NOT NULL,
  PRIMARY KEY (`chat_uuid`),
  KEY `name` (`name`),
  KEY `type` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Chat "directory"';

-- Tietojen vientiä ei oltu valittu.
-- Dumping structure for taulu reaktor_chat_service_app.chat_participations
CREATE TABLE IF NOT EXISTS `chat_participations` (
  `user_uuid` binary(16) NOT NULL,
  `chat_uuid` binary(16) NOT NULL,
  `is_admin` bit(1) NOT NULL DEFAULT b'0',
  `is_creator` bit(1) NOT NULL DEFAULT b'0',
  `is_banned` bit(1) NOT NULL DEFAULT b'0',
  `joined_timestamp` int(10) unsigned NOT NULL,
  PRIMARY KEY (`user_uuid`,`chat_uuid`),
  KEY `FK_chat_participations_chats` (`chat_uuid`),
  CONSTRAINT `FK_chat_participations_chats` FOREIGN KEY (`chat_uuid`) REFERENCES `chats` (`chat_uuid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='A table to link users and chats together, while also specifying the rights for the user to the chat.';

-- Tietojen vientiä ei oltu valittu.
-- Dumping structure for function reaktor_chat_service_app.CONV_BIN_TXT_UUID
DELIMITER //
CREATE DEFINER=`root`@`localhost` FUNCTION `CONV_BIN_TXT_UUID`(`BIN_UUID` BINARY(16)) RETURNS char(36) CHARSET utf8
    NO SQL
BEGIN
RETURN concat(HEX(LEFT(BIN_UUID,4)),'-', HEX(MID(BIN_UUID,5,2)),'-', HEX(MID(BIN_UUID,7,2)),'-',HEX(MID(BIN_UUID,9,2)),'-',HEX(RIGHT(BIN_UUID,6))); 
END//
DELIMITER ;

-- Dumping structure for function reaktor_chat_service_app.CONV_TXT_BIN_UUID
DELIMITER //
CREATE DEFINER=`root`@`localhost` FUNCTION `CONV_TXT_BIN_UUID`(`TXT_UUID` CHAR(36)) RETURNS binary(16)
    NO SQL
BEGIN
RETURN CONCAT(UNHEX(LEFT(TXT_UUID,8)),UNHEX(MID(TXT_UUID,10,4)),UNHEX(MID(TXT_UUID,15,4)),UNHEX(MID(TXT_UUID,20,4)),UNHEX(RIGHT(TXT_UUID,12))); 
END//
DELIMITER ;

-- Dumping structure for function reaktor_chat_service_app.GEN_BIN_UUID
DELIMITER //
CREATE DEFINER=`root`@`localhost` FUNCTION `GEN_BIN_UUID`() RETURNS binary(16)
    NO SQL
BEGIN
DECLARE TXT_UUID char(37);
SET TXT_UUID = UUID();
RETURN CONV_TXT_BIN_UUID(TXT_UUID);
END//
DELIMITER ;

-- Dumping structure for taulu reaktor_chat_service_app.increments
CREATE TABLE IF NOT EXISTS `increments` (
  `val` int(11) NOT NULL DEFAULT '0',
  `type` varchar(50) NOT NULL,
  PRIMARY KEY (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Tietojen vientiä ei oltu valittu.
-- Dumping structure for taulu reaktor_chat_service_app.messages
CREATE TABLE IF NOT EXISTS `messages` (
  `message_uuid` binary(16) NOT NULL,
  `user_uuid` binary(16) NOT NULL,
  `chat_uuid` binary(16) NOT NULL,
  `timestamp` int(11) NOT NULL,
  `text` text COLLATE utf8_unicode_ci NOT NULL,
  `is_removed` bit(1) NOT NULL,
  PRIMARY KEY (`message_uuid`),
  KEY `user_uuid_chat_uuid` (`user_uuid`,`chat_uuid`),
  KEY `timestamp` (`timestamp`),
  KEY `chat_uuid` (`chat_uuid`),
  KEY `user_uuid` (`user_uuid`),
  CONSTRAINT `FK_messages_chats` FOREIGN KEY (`chat_uuid`) REFERENCES `chats` (`chat_uuid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='A table to hold all messages.';

-- Tietojen vientiä ei oltu valittu.
-- Dumping structure for taulu reaktor_chat_service_app.regToken
CREATE TABLE IF NOT EXISTS `regToken` (
  `token` varchar(10) NOT NULL,
  PRIMARY KEY (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Tietojen vientiä ei oltu valittu.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
